import React, {useReducer} from 'react';
import axios from 'axios';
import GithubReducer from './githubReducer';
import {
    SEARCH_USERS,
    SET_LOADING,
    CLEAR_USERS,
    GET_USER,
    GET_REPOS
} from '../types';
import githubContext from './githubContext';

let githubClientID;
let githubClientSecret;

if(process.env.NODE_ENV !== 'production'){
  githubClientID = process.env.REACT_APP_GITHUB_CLIENT_ID;
  githubClientSecret = process.env.REACT_APP_GITHUB_CLIENT_SECRET;
} else {
  githubClientID = process.env.GITHUB_CLIENT_ID;
  githubClientSecret = process.env.GITHUB_CLIENT_SECRET;
}

const GithubState = props => {
    const initialState = {
        users: [],
        user: {},
        repos: [],
        loading: false
    }

    const [state, dispatch] = useReducer(GithubReducer, initialState);

    //Return users matching the specified search
    const searchUsers = async text => {

        setLoading(true);
        const result = await axios.get(`https://api.github.com/search/users?q=${text}&client_id=
          ${githubClientID}&client_secret=
          ${githubClientSecret}`);
    
        dispatch({type: SEARCH_USERS, payload: result.data.items});
    }

    //Clear all users from the state
    const clearUsers = () => {
        dispatch({type: CLEAR_USERS});
      }    

    //Set loading state to true
    const setLoading = () => {
        dispatch({type: SET_LOADING});
    }

    //Add info of the specified user to state
    const getUser = async username => {

        setLoading();
        const result = await axios.get(`https://api.github.com/users/${username}?client_id=
          ${githubClientID}&client_secret=
          ${githubClientSecret}`);
    
        dispatch({type: GET_USER, payload: result.data});
      }

      const getUserRepos = async username => {

        setLoading();
        const result = await axios.get(`https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc&client_id=
          ${githubClientID}&client_secret=
          ${githubClientSecret}`);
    
        dispatch({type: GET_REPOS, payload: result.data});
      }

    return <githubContext.Provider
        value={{
            users: state.users,
            user: state.user,
            repos: state.repos,
            loading: state.loading,
            searchUsers,
            clearUsers,
            getUser,
            getUserRepos
        }}
    >
        {props.children}
    </githubContext.Provider>
}

export default GithubState